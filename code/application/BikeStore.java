/*
 * Student Name: Shakela Hossain
 * StudentID: 1435730
 */
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bike = new Bicycle[4];
        bike[0] = new Bicycle("SomeRandomCompany", 21, 50);
        bike[1] = new Bicycle("IDontKnowAnythingAboutBikes", 30, 80);
        bike[2] = new Bicycle("SomeGreatCompany", 44, 120);
        bike[3] = new Bicycle("SomeOtherCompany", 20, 55);
        System.out.println(printer(bike));
    }
    private static String printer(Bicycle[] bike){
        String listOfBikes = "";
        for (int i = 0; i < bike.length ; i++){
            listOfBikes += bike[i].toString() + "\n";
        }
        return listOfBikes;
    }
    
}
