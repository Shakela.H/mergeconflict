/*
 * Student Name: Shakela Hossain
 * StudentID: 1435730
 */

package vehicles;

public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public Bicycle(String newManufacturer, int newNumberGears, double newMaxSpeed){
        this.manufacturer = newManufacturer;
        this.numberGears = newNumberGears;
        this.maxSpeed = newMaxSpeed;
    }

    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
    }
}